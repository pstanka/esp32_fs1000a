from machine import Pin
import time
import gc
p4=Pin(4,Pin.OUT)
BIT_TRANSMIT_TIME=200
def run(msg):
    #gc.disable()
    global p4
    bs = msg.encode("ascii")
    bit_masks = [1,2,4,8,16,32,64,128]
    print("Loop starting")
    start_ticks = time.ticks_us()
    for bit_offset in range(0, len(bs)*8):
        bajt_offset = bit_offset >> 3
        bit_in_bajt_offset = bit_offset & 7
        bajt = bs[bajt_offset]
        bit_mask = bit_masks[bit_in_bajt_offset]
        tt_now = time.ticks_us()
        time.sleep_us(start_ticks + BIT_TRANSMIT_TIME * (bit_offset + 1) - tt_now)
        p4.value(bajt & bit_mask != 0)
    #gc.enable()
    print("sleep1+: ", (start_ticks + BIT_TRANSMIT_TIME * len(bs)*8 - time.ticks_us()))
    print("sleep11+: ", (time.ticks_us()) - start_ticks)
    print("Loop finished")

